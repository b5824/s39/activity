const mongoose = require('mongoose');

//NOTE: User schema
let userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First name cannot be empty'],
    },
    lastName: {
        type: String,
        required: [true, 'Last name cannot be empty'],
    },
    email: {
        type: String,
        required: [true, 'Email cannot be empty'],
    },
    password: {
        type: String,
        required: [true, 'Password cannot be empty'],
    },
    mobileNo: {
        type: String,
        required: [true, 'Mobile number cannot be empty'],
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    enrollments: [{
        courseId: {
            type: String,
            required: [true, 'Couse Id cannot be empty'],
        },
        status: {
            type: String,
            default: 'Enrolled',
        },
        dateEnrolled: {
            type: Date,
            default: new Date(),
        },
    }, ],
});

//NOTE: Exporting user schema and creating mongoose model
module.exports = mongoose.model('User', userSchema);