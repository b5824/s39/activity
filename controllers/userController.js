//NOTE: Dependencies
const User = require('../models/User');
const bcrypt = require('bcryptjs');
const auth = require('../auth');

//NOTE: Register a user
let registerUser = (req, res) => {
  console.log(req.body);

  /*
				bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)

				salt rounds - number of times the cahracters inthe hash is randomized
						*/
  User.countDocuments({ email: req.body.email }, (err, count) => {
    if (count === 0) {
      const hashedPW = bcrypt.hashSync(req.body.password, 10);

      let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        mobileNo: req.body.mobileNo,
        password: hashedPW,
      });

      newUser
        .save()
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
    } else {
      return res.send('Email already exists!');
    }
  });
};

//NOTE: Retrieval of ALL users
let getAllUsers = (req, res) => {
  User.find({})
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

let loginUser = (req, res) => {
  console.log(req.body);

  /*
		1. find the user by the email
		2. if user is found, check the password if it matches
		3. if user is not found then send a message to the client that user does not exist
		4. if users' password matches the user inputs' password then generate the "token/key" to access our app. Otherwise, send a message to the client and do not grant them access
*/

  User.findOne({ email: req.body.email })
    .then((foundUser) => {
      if (foundUser === null) {
        return res.send('No user found in the database');
      } else {
        //companySync - allows us to compare what the user has entered and the information found in the database
        //compareSync - returns a boolean value. If it matches, it will return true, otherwise, it will return false.
        const isPasswordCorrect = bcrypt.compareSync(
          req.body.password,
          foundUser.password
        );

        console.log(isPasswordCorrect);

        if (isPasswordCorrect) {
          //Creating a property and the value is taken from the auth.
          return res.send({ accessToken: auth.createAcessToken(foundUser) });
        } else {
          return res.send('Incorrect password, please try agan');
        }
      }
    })
    .catch((err) => res.send(err));
};

let getUserDetails = (req, res) => {
  console.log(req.user);
  /* expected output: decoded token
        {
        id: '629700ee225f97710a999ec2',
        email: 'tine@test.com',
        isAdmin: false,
        iat: 1654136818
        }
    */

  //find the logged in user's document from out dband send it to the clien by its id
  User.findById(req.user.id)
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

let checkEmailExists = (req, res) => {
  User.countDocuments({ email: req.body.email }, (err, count) => {
    if (count === 0) {
      return res.send('Email is available');
    } else {
      User.find({ email: req.body.email })
        .then(() => {
          res.send('Email is not available');
        })
        .catch((err) => res.send(err));
    }
  });
};

module.exports = {
  registerUser,
  getAllUsers,
  getUserDetails,
  loginUser,
  checkEmailExists,
};
