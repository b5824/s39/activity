const express = require('express');
const router = express.Router();
const auth = require('../auth');

const courseControllers = require('../controllers/courseController');

const { verify, verifyAdmin } = auth;

//Upon verifying that the user is logged in and is the value of isAdmin is true then execute the controller
router.post('/', verify, verifyAdmin, courseControllers.addCourse);
router.get('/', courseControllers.getAllCourses);
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);

module.exports = router;
